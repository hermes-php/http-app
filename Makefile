PHP=/usr/bin/env php
WGET=wget -q -nc -O

PHPSTAN=phpstan.phar
PHPSTAN_URL=https://github.com/phpstan/phpstan/releases/download/0.11.2/phpstan.phar

PHPUNIT=phpunit.phar
PHPUNIT_URL=https://phar.phpunit.de/phpunit-7.phar

PHPCS=php-cs-fixer.phar
PHPCS_URL=https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/download/v2.14.2/php-cs-fixer.phar

PHPMD=phpmd.phar
PHPMD_URL=http://static.phpmd.org/php/latest/phpmd.phar

phpunit:
	$(WGET) $(PHPUNIT) $(PHPUNIT_URL) || true

phpstan:
	$(WGET) $(PHPSTAN) $(PHPSTAN_URL) || true

phpcs:
	$(WGET) $(PHPCS) $(PHPCS_URL) || true

phpmd:
	$(WGET) $(PHPMD) $(PHPMD_URL) || true

analysis: phpstan
	$(PHP) $(PHPSTAN) analyze --level 7 src/

quality: phpmd
	$(PHP) $(PHPMD) src/ text cleancode,codesize,controversial,design,naming,unusedcode

tests: phpunit
	$(PHP) $(PHPUNIT) --coverage-text

tests-clean: phpunit
	$(PHP) $(PHPUNIT) --coverage-text --colors=never

changelog:
	./.changelog.sh

style-fix: phpcs
	$(PHP) $(PHPCS) fix --diff --verbose

style-check: phpcs
	$(PHP) $(PHPCS) fix --dry-run --diff --verbose

commit: style-fix analysis quality tests changelog
	git add . && git commit
