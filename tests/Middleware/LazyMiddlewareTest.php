<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp\Tests\Middleware;

use Hermes\HttpApp\Middleware\LazyMiddleware;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class LazyMiddlewareTest.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class LazyMiddlewareTest extends TestCase
{
    public function testThatLazyMiddlewareWorksWithMiddlewareInstance(): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $middleware = $this->createMock(MiddlewareInterface::class);
        $request = $this->createMock(ServerRequestInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $handler = $this->createMock(RequestHandlerInterface::class);

        $container->expects($this->once())
            ->method('get')
            ->with('lazy-middleware')
            ->willReturn($middleware);
        $middleware->expects($this->once())
            ->method('process')
            ->with($request, $handler)
            ->willReturn($response);

        $lazyMiddleware = new LazyMiddleware($container, 'lazy-middleware');
        $response = $lazyMiddleware->process($request, $handler);
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testThatLazyMiddlewareWorksWithRequestHandler(): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $decoratedHandler = $this->createMock(RequestHandlerInterface::class);
        $request = $this->createMock(ServerRequestInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $handler = $this->createMock(RequestHandlerInterface::class);

        $container->expects($this->once())
            ->method('get')
            ->with('lazy-middleware')
            ->willReturn($decoratedHandler);
        $decoratedHandler->expects($this->once())
            ->method('handle')
            ->with($request)
            ->willReturn($response);

        $lazyMiddleware = new LazyMiddleware($container, 'lazy-middleware');
        $response = $lazyMiddleware->process($request, $handler);
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testThatLazyMiddlewareWorksWithCallable(): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $callable = $this->createPartialMock(\stdClass::class, ['__invoke']);
        $request = $this->createMock(ServerRequestInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $handler = $this->createMock(RequestHandlerInterface::class);

        $container->expects($this->once())
            ->method('get')
            ->with('lazy-middleware')
            ->willReturn($callable);
        $callable->expects($this->once())
            ->method('__invoke')
            ->with($request, $handler)
            ->willReturn($response);

        $lazyMiddleware = new LazyMiddleware($container, 'lazy-middleware');
        $response = $lazyMiddleware->process($request, $handler);
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testThatLazyMiddlewareFailsWithInvalidService(): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $wrongService = new DummyService();
        $request = $this->createMock(ServerRequestInterface::class);
        $handler = $this->createMock(RequestHandlerInterface::class);

        $container->expects($this->once())
            ->method('get')
            ->with('lazy-middleware')
            ->willReturn($wrongService);

        $lazyMiddleware = new LazyMiddleware($container, 'lazy-middleware');

        $this->expectException(\InvalidArgumentException::class);
        $lazyMiddleware->process($request, $handler);
    }
}

class DummyService
{
}
