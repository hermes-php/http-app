How To Contribute
=================
Thank you for your interest in contributing to this project. Here are some ways
you can help.

## Support
Sometimes people open issues asking certain questions about how to do certain
things involving this library. We label these issues with the `support` tag.
If you are well versed in the use of this library, you can help by answering the
questions in those tags.

## Feature Requests
We welcome any feature requests in this library. You can open an issue with your
proposed changes explaining how this library will benefit of that feature. After
it is properly discussed and approved, either you or the maintainers will work on
an implementation and submit a merge request. This merge request will be merged if
approved.

## Merge Request Process
We welcome any contributions and bugfixes to this library. However, we follow a
very strict Merge Request process. These are the minimum requirements for a Merge
Request to be accepted:
- All changes must be squashed in one commit describing the feature accordingly.
- It must be able to be merged with a simple fast-forward. This is to keep a linear history.
If you are not able to fast-forward, you must rebase from master.
- All changes must be properly tested. `make tests`
- Changelog must be updated with the commit `make changelog`
- Code style must be properly applied `make style-fix`
- Static analysis tools must not throw errors.