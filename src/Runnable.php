<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp;

/**
 * Interface Runnable.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface Runnable extends AppInterface
{
    /**
     * Runs the application.
     */
    public function run(): void;
}
