<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp\Middleware;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Stratigility\Middleware\CallableMiddlewareDecorator;
use Zend\Stratigility\Middleware\RequestHandlerMiddleware;

/**
 * Class LazyMiddleware.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class LazyMiddleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var string
     */
    protected $serviceName;

    /**
     * LazyMiddleware constructor.
     *
     * @param ContainerInterface $container
     * @param string             $serviceName
     */
    public function __construct(ContainerInterface $container, string $serviceName)
    {
        $this->container = $container;
        $this->serviceName = $serviceName;
    }

    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->getMiddleware()->process($request, $handler);
    }

    protected function getMiddleware(): MiddlewareInterface
    {
        $middleware = $this->container->get($this->serviceName);

        if ($middleware instanceof MiddlewareInterface) {
            return $middleware;
        }

        if ($middleware instanceof RequestHandlerInterface) {
            return new RequestHandlerMiddleware($middleware);
        }

        if (is_callable($middleware)) {
            return new CallableMiddlewareDecorator($middleware);
        }

        throw new \InvalidArgumentException(sprintf(
            'The service "%s" is an invalid middleware. Must be a callable, or instances of %s or %s',
            $this->serviceName,
            MiddlewareInterface::class,
            RequestHandlerInterface::class
        ));
    }
}
