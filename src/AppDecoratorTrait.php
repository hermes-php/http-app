<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\Route;

/**
 * Trait AppDecoratorTrait.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
trait AppDecoratorTrait
{
    /**
     * @var AppInterface
     */
    protected $app;

    public function pipe($middleware, string $path = null): void
    {
        $this->app->pipe($middleware, $path);
    }

    public function get(string $path, $middleware, string $name = null): Route
    {
        return $this->app->get($path, $middleware, $name);
    }

    public function post(string $path, $middleware, string $name = null): Route
    {
        return $this->app->post($path, $middleware, $name);
    }

    public function put(string $path, $middleware, string $name = null): Route
    {
        return $this->app->put($path, $middleware, $name);
    }

    public function patch(string $path, $middleware, string $name = null): Route
    {
        return $this->app->patch($path, $middleware, $name);
    }

    public function delete(string $path, $middleware, string $name = null): Route
    {
        return $this->app->delete($path, $middleware, $name);
    }

    public function any(string $path, $middleware, string $name = null): Route
    {
        return $this->app->any($path, $middleware, $name);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->app->handle($request);
    }
}
