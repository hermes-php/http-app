<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\Route;
use Zend\Expressive\Router\RouteCollector;
use Zend\Stratigility\Middleware as ZendMiddleware;
use Zend\Stratigility\MiddlewarePipeInterface;

/**
 * Class Application.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class App implements AppInterface
{
    use ResolverTrait;

    /**
     * @var RouteCollector
     */
    protected $collector;
    /**
     * @var MiddlewarePipeInterface
     */
    protected $pipeline;

    /**
     * Application constructor.
     *
     * @param RouteCollector          $collector
     * @param MiddlewarePipeInterface $pipeline
     */
    public function __construct(RouteCollector $collector, MiddlewarePipeInterface $pipeline)
    {
        $this->collector = $collector;
        $this->pipeline = $pipeline;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->pipeline->handle($request);
    }

    /**
     * {@inheritdoc}
     */
    public function pipe($middleware, string $path = null): void
    {
        $middleware = $this->resolve($middleware);
        if (null !== $path) {
            $middleware = new ZendMiddleware\PathMiddlewareDecorator($path, $middleware);
        }
        $this->pipeline->pipe($middleware);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $path, $middleware, string $name = null): Route
    {
        return $this->collector->get($path, $this->resolve($middleware), $name);
    }

    /**
     * {@inheritdoc}
     */
    public function post(string $path, $middleware, string $name = null): Route
    {
        return $this->collector->post($path, $this->resolve($middleware), $name);
    }

    /**
     * {@inheritdoc}
     */
    public function put(string $path, $middleware, string $name = null): Route
    {
        return $this->collector->put($path, $this->resolve($middleware), $name);
    }

    /**
     * {@inheritdoc}
     */
    public function patch(string $path, $middleware, string $name = null): Route
    {
        return $this->collector->patch($path, $this->resolve($middleware), $name);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(string $path, $middleware, string $name = null): Route
    {
        return $this->collector->delete($path, $this->resolve($middleware), $name);
    }

    /**
     * {@inheritdoc}
     */
    public function any(string $path, $middleware, string $name = null): Route
    {
        return $this->collector->any($path, $this->resolve($middleware), $name);
    }
}
