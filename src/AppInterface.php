<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Router\Route;

/**
 * Interface AppInterface.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface AppInterface extends RequestHandlerInterface
{
    /**
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @param string|null                                                 $path
     */
    public function pipe($middleware, string $path = null): void;

    /**
     * @param string                                                      $path
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @param string|null                                                 $name       the name of the route
     *
     * @return Route
     */
    public function get(string $path, $middleware, string $name = null): Route;

    /**
     * @param string                                                      $path
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @param string|null                                                 $name       the name of the route
     *
     * @return Route
     */
    public function post(string $path, $middleware, string $name = null): Route;

    /**
     * @param string                                                      $path
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @param string|null                                                 $name       the name of the route
     *
     * @return Route
     */
    public function put(string $path, $middleware, string $name = null): Route;

    /**
     * @param string                                                      $path
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @param string|null                                                 $name       the name of the route
     *
     * @return Route
     */
    public function patch(string $path, $middleware, string $name = null): Route;

    /**
     * @param string                                                      $path
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @param string|null                                                 $name       the name of the route
     *
     * @return Route
     */
    public function delete(string $path, $middleware, string $name = null): Route;

    /**
     * @param string                                                      $path
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     * @param string|null                                                 $name       the name of the route
     *
     * @return Route
     */
    public function any(string $path, $middleware, string $name = null): Route;
}
