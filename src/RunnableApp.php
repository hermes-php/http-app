<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class RunnableAppDecorator.
 *
 * This RunnableApp decorates an instance of AppInterface and  takes two callables
 * as extra arguments, one for creating an instance of ServerRequestInterface
 * and the other one for emitting a ResponseInterface.
 *
 * Implementation details of this are up to you.
 *
 * Callables are wrapped into private methods to ensure type safety.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class RunnableApp implements Runnable
{
    use AppDecoratorTrait;

    /**
     * @var callable
     */
    private $serverRequestFactory;
    /**
     * @var callable
     */
    private $responseEmitter;

    /**
     * RunnableApp constructor.
     *
     * @param AppInterface $app
     * @param callable     $serverRequestFactory
     * @param callable     $responseEmitter
     */
    public function __construct(AppInterface $app, callable $serverRequestFactory, callable $responseEmitter)
    {
        $this->app = $app;
        $this->serverRequestFactory = $serverRequestFactory;
        $this->responseEmitter = $responseEmitter;
    }

    /**
     * Runs the application using the regular PHP SAPI.
     */
    public function run(): void
    {
        $this->emitResponse($this->handle($this->createRequest()));
    }

    /**
     * @return ServerRequestInterface
     */
    private function createRequest(): ServerRequestInterface
    {
        return ($this->serverRequestFactory)();
    }

    /**
     * @param ResponseInterface $response
     */
    private function emitResponse(ResponseInterface $response): void
    {
        ($this->responseEmitter)($response);
    }
}
