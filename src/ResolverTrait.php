<?php

/*
 * This file is part of the Hermes\HttpApp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\HttpApp;

use Hermes\HttpApp\Middleware\LazyMiddleware;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Stratigility\Middleware as ZendMiddleware;

/**
 * Class ResolverTrait.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
trait ResolverTrait
{
    /**
     * @var ContainerInterface|null
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    /**
     * @param string|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     *
     * @return MiddlewareInterface
     */
    protected function resolve($middleware): MiddlewareInterface
    {
        if ($middleware instanceof MiddlewareInterface) {
            return $middleware;
        }
        if ($middleware instanceof RequestHandlerInterface) {
            return new ZendMiddleware\RequestHandlerMiddleware($middleware);
        }
        if (is_callable($middleware)) {
            return new ZendMiddleware\CallableMiddlewareDecorator($middleware);
        }
        if (is_string($middleware) && null !== $this->container && $this->container->has($middleware)) {
            return new LazyMiddleware($this->container, $middleware);
        }
        throw new \InvalidArgumentException(sprintf('Cannot resolve "%s"', $middleware));
    }
}
