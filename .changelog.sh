#!/usr/bin/env bash

git ls-files -m | grep 'CHANGELOG.md' &> /dev/null
if [ $? == 0 ]; then
    exit 0; else
    read -p "Changelog has not been modified. Continue anyway? (Y/n): " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        exit 1; else
        exit 0;
    fi
fi